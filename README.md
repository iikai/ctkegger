# README #

### CTKegger ###

* Quick proof-of-concept for private event beer pour measuring. Based on Kegomatic by Adafruit
* Works with Python3 on Raspberry Pi
* Required pip3 packages:
    * `mysql`, tested with 0.0.2
    * `mysql-connector`, tested with 2.2.9
    * `mysqlclient`, tested with 2.0.1
    * `pifacecommon`, tested with 4.2.2
    * `pifacedigitalio`, tested with 3.0.5
    * `redis`, tested with 3.5.3
    * `schedule`, tested with 0.6.0

### SQL Setup ###

#### Create Database option 1 ####
```
$ sudo mysql -uroot -p ctkegger
```
In SQL console
```
CREATE DATABASE ctkegger;
CREATE USER 'ctkegbot'@'localhost' IDENTIFIED BY 'KALJAAkurkusta';
GRANT ALL PRIVILEGES ON ctkegger.* TO 'ctkegbot'@'localhost';
GRANT FILE ON *.* to 'ctkegbot'@'localhost';
FLUSH PRIVILEGES;
```

#### Create Database option 2 ####
Browse to `/db/` folder and use below script to create db schema with empty kegger db.
Dummy taps and Thief - rows are created to db.
```
./createDBScheme_empty.sh
```
PS. Remember to make backups before executing any scripts!

#### Make SQL dumpfiles option 1 ####
In ctkegger/db folder run:
```
$ mysql -u ctkegbot -p ctkegger < makeDumps.sql
```
Dumpfiles will be created into `/tmp/` -folder in the filesystem root.
They need to be moved into a new folder under ctkegger/db/csv_dumps, like `CT21`.
CSV-dumpfiles can be edited and appended but the columns cannot be changed.

#### Make SQL dumpfiles option 2 ####
In ctkegger/db folder run:
```
$ ./backupCurrentDB.sh
```
Will run mysqldump and dump db+schema to `./db/database_dumps/` -folder and do table dumps like described in option 1.

### REDIS Setup ###
In user home folder (or somewhere outside Git-repo)
```
$ wget http://download.redis.io/redis-stable.tar.gz
$ tar xvzf redis-stable.tar.gz
$ cd redis-stable
$ make
$ sudo make install
$ make test
$ redis-cli ping
  => PONG
```
To start the server
```
$ redis-server
```


## Starting app up ##
The startup script automates starting the apps and also outputs all output and erros and prints to separate logfiles in `/logs/`

Startupscript can be invoked by 
```
$ ./startAll.sh
```
nb. redis logs are not redirected to `/logs/`

if you get error that it is not an executable file, give +x permissions with
```
$ chmod +x startAll.sh
```

## Development related ##

### To do: ###

* REDIS Server setup for custom endpoint call
* Service for custom endpoint calls
* Alert bot for system alerts

### Bot ideas ###

* Thirsty first -> Announce First drinker each day after 12 o'clock
* First blood  -> Announce First pour from New Keg
* Loppasuu / Quick drinker  -> Announce drinker if three pours (> 0.2l) in 60 minutes
* Max Pour -> Announce new largest pour
* Thief on tap -> No rfid has been read on pour
* New highest BAC -> REDIS has the highes BAC DrinkerId, when it changes after BAC-calculus it creates a notification
* New biggest volume consumpiton -> REDIS has the highes consumption DrinkerId, when it changes after pour it creates a notification
