import pygame, sys
from pygame.locals import *
from random import randint

class ollibot():
  image = ''
  x = 0
  y = 0
  ll = 0 # left limit
  rl = 0 # right limit
  tl = 0 # top limit
  bl = 0 # bottom limit
  angle = 0
  enabled = False
  #xmove = 5
  #ymove = 5

  xdirection = 'right'
  ydirection = 'down'

  def __init__(self,image_file, x, y, ll, rl, tl, bl):
    self.image = pygame.image.load(image_file)
    self.orig_image = pygame.image.load(image_file)
    self.image = self.image.convert_alpha()
    self.x = x
    self.y = y
    self.ll = ll
    self.rl = rl
    self.tl = tl
    self.bl = bl
    self.xmove = randint(5,10)
    self.ymove = randint(5,10)
    self.angle = 0
    self.rotate = -4
    self.enabled = False

  def update(self):

    if (self.xdirection == 'right'):
      self.x += self.xmove
    elif (self.xdirection == 'left'):
      self.x -= self.xmove
    if (self.ydirection == 'up'):
      self.y -= self.ymove
    elif (self.ydirection == 'down'):
      self.y += self.ymove

    if (self.x > self.rl and self.xdirection != 'left'):
        self.xmove = randint(5,10)
        self.xdirection = 'left'
        self.rotate = self.rotate*-1
    elif (self.x < self.ll and self.xdirection != 'right'):
        self.xdirection = 'right'
        self.xmove = randint(5,10)
        self.rotate = self.rotate*-1
    if (self.y > self.bl):
        self.ymove = randint(5,10)
        self.rotate = self.rotate*-1
        self.ydirection = 'up'
    elif (self.y < self.tl):
        self.ymove = randint(5,10)
        self.rotate = self.rotate*-1
        self.ydirection = 'down'

    self.angle += self.rotate % 360
    self.image = rotate_image(self.orig_image, self.angle)

def rotate_image(image, angle):
    """rotate a Surface, maintaining position."""
    loc = image.get_rect().center  #rot_image is not defined
    rot_sprite = pygame.transform.rotozoom(image, angle, 1)
    rot_sprite.get_rect().center = loc
    return rot_sprite
