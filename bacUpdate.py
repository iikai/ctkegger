# -*- coding: iso-8859-1 -*-
import sys
import schedule
import time
import datetime
import math
import json
import redis
import numpy as np
import utils.apiCalls as ac
import utils.sql_queries as sq
import utils.constants as const
from utils.dbConnection import Database

# set up database
bacCnx = Database('ctkegger')

# set up REDIS
redisServer = redis.Redis(host='localhost', port=6379, db=0)
print('started bacUpdate.py.')

def updateBAC():
    print('Starting to run updateBAC.')
    bac_records = []
    span_serving = const.BAC_SPAN_SERVING_TIME_STEPS  # step
    time_step = const.BAC_TIME_STEP_MINUTE  # minute / step
    water_in_blood = 0.806
    body_water_constant = 0.58  # value for a MALE
    metabolism_rate = 0.15  # permille/h
    time_now = datetime.datetime.now()
    current_time = time_now.strftime('%Y-%m-%d %H:%M:00')
    begin_time = time_now - datetime.timedelta(days=const.BAC_MAX_INTERVAL_DAY)
    seconds_in_minute = 60
    minutes_in_hour = 60
    hours_in_day = 24
    permille_in_percent = 10

    args = [current_time, ]
    query = f'select * from Drinker where id in ( select distinct drinker_id from Serving where drank_at > %s - INTERVAL {const.BAC_MAX_INTERVAL_DAY} DAY) and id > {const.NO_USER_ID}'
    bacCnx.execute(query, args)
    drinker_rows = bacCnx.fetchall()
    # print('Total Row(s):', len(drinkerRows))
    for row in drinker_rows:
        args = [row['id'], current_time]
        serving_query = f'select * from Serving where drinker_id = %s and drank_at > %s - INTERVAL {const.BAC_MAX_INTERVAL_DAY} DAY'
        bacCnx.execute(serving_query, args)
        serving_rows = bacCnx.fetchall()
        bac_history = np.zeros(int(const.BAC_MAX_INTERVAL_DAY * hours_in_day * minutes_in_hour / time_step))  # all steps
        drinker_id = (row['id'])
        drinker_name = (row['name'])
        mass = row['weight']  # kg

        # History of alcohol intake
        for srow in serving_rows:
            # https://en.wikipedia.org/wiki/Blood_alcohol_content#Estimation_by_intake
            servings = srow['servings']  # 12g of pure alcohol
            delta_serving = (float(servings) * water_in_blood / body_water_constant / mass * permille_in_percent) * (1 / float(span_serving))  # permille / time step
            delta_time_second = float((srow['drank_at'] - begin_time).total_seconds())  # second
            starting_step_for_drink = int(math.floor((delta_time_second/seconds_in_minute) / time_step))  # step
            if starting_step_for_drink < 0:
                starting_step_for_drink = 0
            for step in range(0, span_serving):
                if starting_step_for_drink+step <= len(bac_history):
                    bac_history[starting_step_for_drink+step:] += delta_serving
                    # bac_history[starting_step_for_drink+step:len(bac_history)] += delta_serving

        # Account for metabolism: remove delta_metabolism from each history
        delta_metabolism = float(metabolism_rate) * (float(time_step)/minutes_in_hour)  # permille / time step
        for i in range(1, len(bac_history)):
            for j in range(i, len(bac_history)):
                bac_history[j] = bac_history[j]-float(delta_metabolism)
                if bac_history[j] < 0:
                    bac_history[j] = 0
                    break

        str_out = str(drinker_id) + "   " + str(drinker_name) + "   " + str(bac_history[-1])
        try:
            print(str_out)
        except:
            print('nimi kusi!')

        bac_value = round(float(bac_history[-1]), 3)
        str_time = str(current_time)
        args = [drinker_id, str_time, bac_value]
        bacQuery = 'insert into BAC (drinker_id, time, BAC) values (%s,%s,%s)'
        bacCnx.execute(bacQuery, args)
        record = sq.get_BAC_for_API(bacCnx.cursor.lastrowid)
        bac_records.append(record)
        sys.stdout.flush()
    else:
        jsonData = bac_records
        redisList = 'bacs'
        if redisServer.llen(redisList) == 0:  # There is no queue in REDIS, all good
            ac.postJsonToAPI(jsonData, True, redisList)
        else:  # There is a queue in REDIS -> Get at the back of the queue
            try:
                print(f'Pushing data to REDIS due to a queue in REDIS (length: {redisServer.llen(redisList)}): {jsonData}')
                redisServer.rpush(redisList, json.dumps(jsonData))
            except Exception as e:
                print(f'Redis server not on! Run command `redis-server` in a separate window. e: {e}')
        return 'All done!'


schedule.every(const.BAC_UPDATE_INTERVAL_MINUTE).minutes.do(updateBAC)

while True:
    schedule.run_pending()
    time.sleep(1)
    sys.stdout.flush()
sys.exit
