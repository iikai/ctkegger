#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.


import logging
import html
import json
import utils.apiCalls as ac
import utils.sql_queries as sq
import utils.constants as const
import utils.helpers as h
from utils.dbConnection import Database
from datetime import datetime
from telegram import Update, ParseMode, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler

# TOKEN = '1249617044:AAFuwoJqvIxHrEVYkqucAdSM1_rvTcnq5vk'

# OWN_CHAT_ID = '1243870043'

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

# Connect to database
bot_cnx = Database(const.DATABASE_NAME)


# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def help_command(update, context):
    """Send a message when the command /help is issued."""
    s = 'CT Kegger bot\n\n' \
        'List of commands:\n' \
        '/taps, /hanat, /whatsintaps: Lists beers currently on taps.\n' \
        '/thief, /thieves, /thiefpours, /listthief, /listthieves, /listthiefpours, /rosvokaadot: Lists thiefs.\n' \
        '/all_drinkers: Lists all drinkers.\n' \
        '/latest_drinkers: List latest dinkers.\n' \
        '/bac: Get latest or yesterdays blood alcohol content.'
    update.message.reply_text(s)


def whats_in_taps(update, context):
    """Bot tells what is currently in taps"""

    def str_single_tap(tap_id, beer_name, abv, total_poured, available_from):
        return f'Tap {tap_id}: {beer_name}\n' \
            f'ABV: {abv} %\n' \
            f'Total amount poured: {total_poured} litres\n' \
            f'Available since: {available_from}'

    taps = sq.whats_in_taps()
    if len(taps) > 0:
        str_out = []
        for tap in taps:
            s = str_single_tap(tap_id=tap['tap_id'],
                               beer_name=tap['beer_name'],
                               abv=tap['abv'],
                               total_poured=tap['total_poured'],
                               available_from=tap['available_from'])
            str_out.append(s)

        separator = '\n\n'
        str_out = separator.join(str_out)
    else:
        str_out = 'No taps found!'
    context.bot.send_message(chat_id=const.TELEGRAM_DEVELOPER_CHAT_ID, text=str_out, parse_mode=ParseMode.HTML)


# def whats_in_taps(update, context):
#     """Bot tells what is currently in taps"""
#     query = 'SELECT ' \
#             't.id AS tap_id, ' \
#             't.currentKegID, ' \
#             'k.name AS beer_name, ' \
#             'k.available_from AS available_from, ' \
#             'k.ABV AS abv, ' \
#             'COALESCE( (SELECT SUM(amount) FROM Pour p WHERE p.keg_id = t.currentKegID), 0) AS total_poured ' \
#             'FROM Tap t ' \
#             'JOIN Keg k ' \
#             'ON t.currentKegId = k.id ' \
#             'ORDER BY t.id ASC'
#     bot_cnx.execute(query)
#
#     def str_single_tap(tap_id, beer_name, abv, total_poured, available_from):
#         return f'Tap {tap_id}: {beer_name}\n' \
#             f'ABV: {abv} %\n' \
#             f'Total amount poured: {total_poured} litres\n' \
#             f'Available since: {available_from}'
#
#
#     str_out = []
#     row = bot_cnx.fetchone()
#     while row is not None:
#         s = str_single_tap(tap_id=row['tap_id'],
#                            beer_name=row['beer_name'],
#                            abv=row['abv'],
#                            total_poured=row['total_poured'],
#                            available_from=row['available_from'])
#         str_out.append(s)
#         row = bot_cnx.fetchone()
#
#     separator = '\n\n'
#     update.message.reply_text(separator.join(str_out))



def list_thief_pours(update, context):
    """Bot lists all thief pours"""

    def str_single_thief_pour(pour_id, tap_id, beer_name, amount, poured_at):
        return f'Pour ID: {pour_id}\n' \
            f'Tap {tap_id}: {beer_name}\n' \
            f'Amount poured: {amount} litres\n' \
            f'Poured at: {poured_at}'

    str_out = ['List of thief pours:']
    thief_pours = sq.list_thief_pours()
    if len(thief_pours) > 0:
        for thief_pour in thief_pours:
            s = str_single_thief_pour(pour_id=thief_pour['pour_id'],
                                      tap_id=thief_pour['tap_id'],
                                      beer_name=thief_pour['beer_name'],
                                      amount=thief_pour['amount'],
                                      poured_at=thief_pour['poured_at'])
            str_out.append(s)

        separator = '\n\n'
        str_out = separator.join(str_out)

        if len(str_out) > const.TELEGRAM_MAX_MESSAGE_LENGTH:
            for x in range(0, len(str_out), const.TELEGRAM_MAX_MESSAGE_LENGTH):
                context.bot.send_message(chat_id=const.TELEGRAM_DEVELOPER_CHAT_ID,
                                         text=str_out[x:x + const.TELEGRAM_MAX_MESSAGE_LENGTH],
                                         parse_mode=ParseMode.HTML)
        else:
            context.bot.send_message(chat_id=const.TELEGRAM_DEVELOPER_CHAT_ID,
                                     text=str_out,
                                     parse_mode=ParseMode.HTML)
    else:
        context.bot.send_message(chat_id=const.TELEGRAM_DEVELOPER_CHAT_ID,
                                 text='No thief pours!',
                                 parse_mode=ParseMode.HTML)


# def list_thief_pours(update, context):
#     """Bot lists all thief pours"""
#     query = 'SELECT ' \
#             'p.keg_id, ' \
#             'p.id AS pour_id, ' \
#             'p.tap_id AS tap_id, ' \
#             'k.name AS beer_name, ' \
#             'p.poured_at AS poured_at, ' \
#             'p.amount AS amount ' \
#             'FROM Pour p ' \
#             'JOIN Keg k ON p.keg_id = k.id ' \
#             f'WHERE p.user_id = {const.NO_USER_ID}'
#     bot_cnx.execute(query)
#
#     def str_single_thief_pour(pour_id, tap_id, beer_name, amount, poured_at):
#         return f'Pour ID: {pour_id}\n' \
#             f'Tap {tap_id}: {beer_name}\n' \
#             f'Amount poured: {amount} litres\n' \
#             f'Poured at: {poured_at}'
#
#
#     str_out = ['List of thief pours:']
#     row = bot_cnx.fetchone()
#     while row is not None:
#         s = str_single_thief_pour(pour_id=row['pour_id'],
#                                   tap_id=row['tap_id'],
#                                   beer_name=row['beer_name'],
#                                   amount=row['amount'],
#                                   poured_at=row['poured_at'])
#         str_out.append(s)
#         row = bot_cnx.fetchone()
#
#     separator = '\n\n'
#     str_out = separator.join(str_out)
#
#     if len(str_out) > const.TELEGRAM_MAX_MESSAGE_LENGTH:
#         for x in range(0, len(str_out), const.TELEGRAM_MAX_MESSAGE_LENGTH):
#             update.message.reply_text(str_out[x:x + const.TELEGRAM_MAX_MESSAGE_LENGTH])
#     else:
#         update.message.reply_text(str_out)


def list_all_drinkers(update, context):
    drinkers = sq.list_all_drinkers()
    if len(drinkers) != 0:
        message = 'Kaikki juojat:\n'
        message += f"<pre>{html.escape('{:<4}'.format('ID'))}| {html.escape('{:<15}'.format('Nimi'))}</pre>\n"
        message += "<pre>----|----------------</pre>\n"
        for drinker in drinkers:
            message += f"<pre>{html.escape('{:<4}'.format(drinker['id']))}| {html.escape(str(drinker['name']))}</pre>\n"
    else:
        message = f'Ei juojia. Surullista..'


    # Finally, send the message
    context.bot.send_message(chat_id=const.TELEGRAM_DEVELOPER_CHAT_ID, text=message, parse_mode=ParseMode.HTML)


def list_drinkers(update, context):
    drinkers = sq.list_recent_drinkers()
    if len(drinkers) != 0:
        message = 'Viimeaikaiset juojat:\n'
        message += f"<pre>{html.escape('{:<4}'.format('ID'))}| {html.escape('{:<15}'.format('Nimi'))}</pre>\n"
        message += "<pre>----|----------------</pre>\n"
        for drinker in drinkers:
            message += f"<pre>{html.escape('{:<4}'.format(drinker['id']))}| {html.escape('{:<15}'.format(drinker['name']))}</pre>\n"
    else:
        message = f'Ei juojia {const.BAC_MAX_INTERVAL_DAY} päivään. Surullista..'


    # Finally, send the message
    context.bot.send_message(chat_id=const.TELEGRAM_DEVELOPER_CHAT_ID, text=message, parse_mode=ParseMode.HTML)


def bac(update, context):
    keyboard = []
    drinkers = sq.list_all_drinkers()
    for drinker_group in list(h.chunks(drinkers, 3)):
        row = []
        for drinker in drinker_group:
            data = {
                'drinker_id': drinker['id'],
                'query': 'bac_drinker'
            }
            row.append(InlineKeyboardButton(drinker['name'], callback_data=json.dumps(data)))

        keyboard.append(row)

    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text('Valitse juoja:', reply_markup=reply_markup)
    # bac = ac.bacNow()

def button(update, context):
    query = update.callback_query
    # CallbackQueries need to be answered, even if no notification to the user is needed
    # Some clients may have trouble otherwise. See https://core.telegram.org/bots/api#callbackquery
    query.answer()
    query_data = json.loads(query.data)

    if query_data['query'] == 'bac_drinker':
        keyboard = []
        options = [
            ['Humalatila nyt', 'bac_now'],
            ['Humalatila eilen parhaimmillaan', 'bac_yesterday']
        ]
        for option in options:
            data = {
                'drinker_id': query_data['drinker_id'],
                'query': option[1]
            }
            keyboard.append([InlineKeyboardButton(option[0], callback_data=json.dumps(data))])

        reply_markup = InlineKeyboardMarkup(keyboard)
        query.edit_message_text(
            text="Valitse toiminto:",
            reply_markup=reply_markup
        )
        return True
    elif query_data['query'] == 'bac_now':
        bac = sq.latest_bac_by_drinker(query_data['drinker_id'])
        # url = const.BAC_NOW_URL
        # bac = ac.commonBacQuery(query_data['drinker_id'], url)
        if 'BAC' in bac:
            if not 'error' in bac:
                # text='Juojan {} humalatila nyt: <b>{}%</b>'.format(bac['name'], bac['bacValue']),
                query.edit_message_text(
                    text=f"Juojan {bac['Name']} humalatila nyt: <b>{round(bac['BAC'], 2)}{const.HTML_PERMILLE}</b>",
                    parse_mode=ParseMode.HTML
                )
            else:
                query.edit_message_text(text=bac['error'])
        else:
            query.edit_message_text(text='{} ei ole juuri nyt humalassa. Juo!'.format(bac['name']))
    elif query_data['query'] == 'bac_yesterday':
        # url = const.BAC_YESTERDAY_URL
        # bac = ac.commonBacQuery(query_data['drinker_id'], url)
        bac = sq.yesterdays_latest_bac_by_drinker(query_data['drinker_id'])
        if 'BAC' in bac:
            if not 'error' in bac:
                # time = datetime.strptime(bac['dateTime'], '%Y-%m-%dT%H:%M:%SZ')
                query.edit_message_text(
                    # text='Juojan {} humalatila eilen, klo {}: <b>{}%</b>'.format(bac['name'], time.strftime('%H.%M'), bac['bacValue']),
                    text='Juojan {} humalatila eilen parhaimmillaan: <b>{}%</b>'.format(bac['Name'], round(bac['BAC'], 2)),
                    parse_mode=ParseMode.HTML
                )
            else:
                query.edit_message_text(text=bac['error'])
        else:
            query.edit_message_text(text='Juojan {} eilisestä humalatilasta ei ole tietoa.'.format(bac['name']))
    else:
        query.edit_message_text(text='Juojan ID: {}'.format(query_data['drinker_id']))

def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(const.TELEGRAM_BOT_ID, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # --------------------- Define commands available for bot ---------------------------

    # Help aliases
    dp.add_handler(CommandHandler("help", help_command))

    # Whats in taps aliases
    dp.add_handler(CommandHandler("whatsintaps", whats_in_taps))
    dp.add_handler(CommandHandler("taps", whats_in_taps))
    dp.add_handler(CommandHandler("hanat", whats_in_taps))

    # List thief pours aliases
    dp.add_handler(CommandHandler("thief", list_thief_pours))
    dp.add_handler(CommandHandler("thieves", list_thief_pours))
    dp.add_handler(CommandHandler("thiefpours", list_thief_pours))
    dp.add_handler(CommandHandler("listthief", list_thief_pours))
    dp.add_handler(CommandHandler("listthieves", list_thief_pours))
    dp.add_handler(CommandHandler("listthiefpours", list_thief_pours))
    dp.add_handler(CommandHandler("rosvokaadot", list_thief_pours))

    # All drinkers aliases
    dp.add_handler(CommandHandler("all_drinkers", list_all_drinkers))

    # Latest drinkers aliases
    dp.add_handler(CommandHandler("latest_drinkers", list_drinkers))

    # Bac aliases
    dp.add_handler(CommandHandler("bac", bac))

    # Button handler
    dp.add_handler(CallbackQueryHandler(button))

    # ----------------------------------------------------------------------------------

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
