-- MySQL dump 10.13  Distrib 5.5.55, for debian-linux-gnu (armv7l)
--
-- Host: localhost    Database: ctkegger
-- ------------------------------------------------------
-- Server version	5.5.55-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `BAC`
--

-- Table structure for table `BAC`
DROP TABLE IF EXISTS `BAC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BAC` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `drinker_id` int(10) NOT NULL,
  `time` datetime NOT NULL,
  `BAC` decimal(3,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `drinker_id` (`drinker_id`),
  CONSTRAINT `BAC_ibfk_1` FOREIGN KEY (`drinker_id`) REFERENCES `Drinker` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16559 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOAD DATA LOCAL INFILE 'CTKegger_Database_Empty/BACs.csv' INTO TABLE BAC FIELDS ENCLOSED BY '"' TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS;

-- Table structure for table `Drinker`
DROP TABLE IF EXISTS `Drinker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Drinker` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `rfid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `weight` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOAD DATA LOCAL INFILE 'CTKegger_Database_Empty/Drinkers.csv' INTO TABLE Drinker FIELDS ENCLOSED BY '"' TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS;

-- Table structure for table `Keg`
DROP TABLE IF EXISTS `Keg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Keg` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `available_from` datetime DEFAULT NULL,
  `available_to` datetime DEFAULT NULL,
  `TapId` int(10) DEFAULT NULL,
  `ABV` decimal(3,1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `TapId` (`TapId`),
  CONSTRAINT `Keg_ibfk_1` FOREIGN KEY (`TapId`) REFERENCES `Tap` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOAD DATA LOCAL INFILE 'CTKegger_Database_Empty/Kegs.csv' INTO TABLE Keg FIELDS ENCLOSED BY '"' TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS;

-- Table structure for table `Pour`
DROP TABLE IF EXISTS `Pour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pour` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `tap_id` int(10) DEFAULT NULL,
  `keg_id` int(10) DEFAULT NULL,
  `poured_at` datetime DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `tap_id` (`tap_id`),
  KEY `keg_id` (`keg_id`),
  CONSTRAINT `Pour_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `Drinker` (`id`),
  CONSTRAINT `Pour_ibfk_2` FOREIGN KEY (`tap_id`) REFERENCES `Tap` (`id`),
  CONSTRAINT `Pour_ibfk_3` FOREIGN KEY (`keg_id`) REFERENCES `Keg` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1344 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOAD DATA LOCAL INFILE 'CTKegger_Database_Empty/Pours.csv' INTO TABLE Pour FIELDS ENCLOSED BY '"' TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS;

-- Table structure for table `Serving`
DROP TABLE IF EXISTS `Serving`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Serving` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `drinker_id` int(10) NOT NULL,
  `drank_at` datetime NOT NULL,
  `servings` decimal(3,1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `drinker_id` (`drinker_id`),
  CONSTRAINT `Serving_ibfk_1` FOREIGN KEY (`drinker_id`) REFERENCES `Drinker` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1400 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOAD DATA LOCAL INFILE 'CTKegger_Database_Empty/Servings.csv' INTO TABLE Serving FIELDS ENCLOSED BY '"' TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS;

-- Table structure for table `Tap`
DROP TABLE IF EXISTS `Tap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tap` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `currentKegId` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `currentKegId` (`currentKegId`),
  CONSTRAINT `Tap_ibfk_1` FOREIGN KEY (`currentKegId`) REFERENCES `Keg` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOAD DATA LOCAL INFILE 'CTKegger_Database_Empty/Taps.csv' INTO TABLE Tap FIELDS ENCLOSED BY '"' TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS;
