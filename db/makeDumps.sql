-- BAC Dump
(SELECT id, drinker_id, time, BAC
FROM BAC
INTO OUTFILE '/tmp/BACs.csv'
FIELDS TERMINATED BY ',' ENCLOSED BY '"' ESCAPED BY '"'
LINES TERMINATED BY '\n');

-- Drinker Dump
(SELECT 'id','rfid','name','weight')
UNION
(SELECT id, rfid, name, weight
FROM Drinker
INTO OUTFILE '/tmp/Drinkers.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ',' ESCAPED BY '"'
LINES TERMINATED BY '\n');

-- Keg Dump
(SELECT 'id','name','available_from','available_to','TapId','ABV')
UNION
(SELECT id, name, available_from, available_to, TapId, ABV
FROM Keg
INTO OUTFILE '/tmp/Kegs.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ',' ESCAPED BY '"'
LINES TERMINATED BY '\n');

-- Pour Dump
(SELECT 'id','user_id','tap_id','keg_id','poured_at','amount')
UNION
(SELECT id, user_id, tap_id, keg_id, poured_at, amount
FROM Pour
INTO OUTFILE '/tmp/Pours.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ',' ESCAPED BY '"'
LINES TERMINATED BY '\n');

-- Serving Dump
(SELECT 'id','drinker_id','drank_at','servings')
UNION
(SELECT id, drinker_id, drank_at, servings
FROM Serving
INTO OUTFILE '/tmp/Servings.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ',' ESCAPED BY '"'
LINES TERMINATED BY '\n');

-- Tap Dump
(SELECT 'id','name','currentKegId')
UNION
(SELECT id, name, currentKegId
FROM Tap
INTO OUTFILE '/tmp/Taps.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ',' ESCAPED BY '"'
LINES TERMINATED BY '\n');
