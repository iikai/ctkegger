#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

from utils.dbConnection import Database
import utils.constants as const

"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

# Connect to database
cnx = Database(const.DATABASE_NAME)


# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')


def help_command(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Help!')


def echo(update, context):
    """Echo the user message."""
    print(update)
    update.message.reply_text(update.message.text)
    print(update)


def whats_in_taps(update, context):
    """Bot tells what is currently in taps"""
    query = 'SELECT t.id AS tapID, t.currentKegID, k.name AS beerName, k.available_from, k.ABV, COALESCE( (SELECT SUM(amount) FROM Pour p WHERE p.keg_id = t.currentKegID), 0) AS total_poured FROM Tap t JOIN Keg k ON t.currentKegId = k.id ORDER BY t.id ASC'
    cnx.execute(query)

    def str_single_tap(tap_id, beer_name, abv, total_poured, available_from):
        return f'Tap {tap_id}: {beer_name}\n' \
            f'ABV: {abv} %\n' \
            f'Total amount poured: {total_poured} litres\n' \
            f'Available since: {available_from}'


    str_out = []
    row = cnx.fetchone()
    while row is not None:
        s = str_single_tap(tap_id=row['tapID'],
                             beer_name=row['beerName'],
                             abv=row['ABV'],
                             total_poured=row['total_poured'],
                             available_from=row['available_from'])
        str_out.append(s)
        row = cnx.fetchone()

    separator = '\n\n'
    update.message.reply_text(separator.join(str_out))


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater('1392074864:AAGzCwu7CBd0mUr2IaFiN1N3PPR359gQGFE', use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help_command))
    dp.add_handler(CommandHandler("whatsintaps", whats_in_taps))
    dp.add_handler(CommandHandler("taps", whats_in_taps))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()