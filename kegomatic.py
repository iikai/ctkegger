#!/usr/bin/python3
import os
import time
import datetime
import pygame
import sys
import random
import pifacedigitalio
import vendor.inputbox as inputbox
import utils.constants as const
import utils.apiCalls as ac
from pygame.locals import *
from utils.flowmeter import FlowMeter
from utils.backdrop import Backdrop
from utils.dbConnection import Database

# Raspberry PI I/O-stuff commented out for testing + import pifacedigitalio!
# Also keys 'u' and 'i' bound for testing purposes
# ----------------- Comment in before using ------------------------------------
pifacedigitalio.init()
pd = pifacedigitalio.PiFaceDigital()
listener = pifacedigitalio.InputEventListener(chip=pd)
# /----------------- Comment in before using -----------------------------------
print('started kegomatic.py.')
# set up database
cnx = Database(const.DATABASE_NAME)

# set up pygame
pygame.init()

# set up the caption
pygame.display.set_caption('KEGBOT')

# hide the mouse
pygame.mouse.set_visible(False)

# set up the flow meters
fm1 = FlowMeter(['Tap 1'], correction_factor=const.FLOW_METER_CORRECTION_FACTORS[0])
fm2 = FlowMeter(['Tap 2'], correction_factor=const.FLOW_METER_CORRECTION_FACTORS[1])
fms = [fm1, fm2]

# set up the window surface
windowSurface = pygame.display.set_mode((const.VIEW_WIDTH, const.VIEW_HEIGHT), 32)
windowInfo = pygame.display.Info()

# set up the fonts
basicFont = pygame.font.Font(const.FONTNAME, const.FONTSIZE)
smallFont = pygame.font.Font(const.FONTNAME, const.SMALLFONTSIZE)
largeFont = pygame.font.Font(const.FONTNAME, const.LARGEFONTSIZE)

# set up the backgrounds
bg = pygame.image.load(const.BG_DEFAULT)
# bg_pour = pygame.image.load(const.BG_POUR)
tweet_bg = pygame.image.load(const.BG_TWEET)
thief_bg = pygame.image.load(const.BG_THIEF)

# Set up backdrop
backdrop = Backdrop(const.BACKDROPS)

# set up help variables
ABV1 = 0.0
ABV2 = 0.0
ABVs = [ABV1, ABV2]
currentBAC = 0.0
currentDrinkerId = const.NO_USER_ID
currentDrinkerName = const.NO_USER_DISPLAY_NAME
currentTap1 = const.TAP_NOT_UPDATED_DISPLAY_NAME
currentTap2 = const.TAP_NOT_UPDATED_DISPLAY_NAME
inputmode = False
lastDrinkerUpdate = 0
lastRender = 0
lastTweet = 0
taps_checked = False
text_buffer = ''
text_for_print = ''
view_mode = const.VIEW_MODE_NORMAL

pour_query = 'insert into Pour (user_id, tap_id, keg_id, poured_at, amount) values (%s,%s,%s,%s,%s)'
serving_query = 'insert into Serving (drinker_id, drank_at, servings) values (%s,%s,%s)'


# draw some text into an area of a surface
# automatically wraps words
# returns any text that didn't get blitted
def drawText(surface, text, color, rect, font, aa=False, bkg=None):
    rect = Rect(rect)
    y = rect.top
    lineSpacing = 7

    # get the height of the font
    fontHeight = font.size('Tg')[1]

    while text:
        i = 1
        # determine if the row of text will be outside our area
        if y + fontHeight > rect.bottom:
            break
        # determine maximum width of line
        while font.size(text[:i])[0] < rect.width and i < len(text):
            i += 1
        # if we've wrapped the text, then adjust the wrap to the last word
        if i < len(text):
            i = text.rfind(' ', 0, i) + 1
        # render the line and blit it to the surface
        if bkg:
            image = font.render(text[:i], 1, color, bkg)
            image.set_colorkey(bkg)
        else:
            image = font.render(text[:i], aa, color)

        surface.blit(image, (rect.left, y))
        y += fontHeight + lineSpacing
        # remove the text we just blitted
        text = text[i:]

    return text


def renderThings(flowMeter, flowMeter2, tweet, windowSurface, basicFont, currentDrinkerName):
    # Clear the screen
    windowSurface.blit(bg, (0, 0))
    # Draw backdrop
    backdrop.update()
    windowSurface.blit(backdrop.image, (backdrop.x, backdrop.y))
    # Draw Tap contents
    text = basicFont.render('BEERS ON TAP', True, const.WHITE, const.BLACK)
    textRect = text.get_rect()
    windowSurface.blit(text, (40, 40))

    if fm1.enabled:
        tap_text = (f'Tap 1: {currentTap1} {str(ABV1)} %')
        text = basicFont.render(tap_text, True, const.WHITE, const.BLACK)
        textRect = text.get_rect()
        windowSurface.blit(text, (40, 40+const.LINEHEIGHT))

    if fm2.enabled:
        tap_text = (f'Tap 2: {currentTap2} {str(ABV2)} %')
        text = basicFont.render(tap_text, True, const.WHITE, const.BLACK)
        textRect = text.get_rect()
        windowSurface.blit(text, (40, 40+(2*(const.LINEHEIGHT))))

    # Draw Amount Poured
    text = basicFont.render('AMOUNT POURED', True, const.WHITE, const.BLACK)
    textRect = text.get_rect()
    windowSurface.blit(text, (40, 200))

    if fm1.enabled:
        tap_text = (f'Tap 1: {fm1.getFormattedThisPour()}')
        text = basicFont.render(tap_text, True, const.WHITE, const.BLACK)
        textRect = text.get_rect()
        windowSurface.blit(text, (40, 200+const.LINEHEIGHT))

    if fm2.enabled:
        tap_text = (f'Tap 2: {fm2.getFormattedThisPour()}')
        text = basicFont.render(tap_text, True, const.WHITE, const.BLACK)
        textRect = text.get_rect()
        windowSurface.blit(text, (40, 200+(2*(const.LINEHEIGHT))))

    # Draw User
    text = basicFont.render('Logged in as...', True, const.WHITE, const.BLACK)
    textRect = text.get_rect()
    windowSurface.blit(text, (windowInfo.current_w / 2 - textRect.width / 2, 380))
    text = largeFont.render(currentDrinkerName, True, const.WHITE, const.BLACK)
    textRect = text.get_rect()
    windowSurface.blit(text, (windowInfo.current_w / 2 - textRect.width / 2, 380+2*const.LINEHEIGHT))

    if currentDrinkerId != 1:
        text = basicFont.render(const.HAIL_TEXT_FOR_LOGGED_IN_USER, True, const.WHITE, const.BLACK)
        textRect = text.get_rect()
        windowSurface.blit(text, (windowInfo.current_w / 2 - textRect.width / 2, 380+5*const.LINEHEIGHT))

    # Draw Keys
    text = smallFont.render(const.KEY_EXPLANATIONS, True, const.WHITE, const.BLACK)
    textRect = text.get_rect()
    windowSurface.blit(text, (windowInfo.current_w / 2 - textRect.width / 2 - 20, const.VIEW_HEIGHT - 60))
    # Draw Amount Poured Total
    text = basicFont.render('TOTAL', True, const.WHITE, const.BLACK)
    textRect = text.get_rect()
    windowSurface.blit(text, (windowInfo.current_w - textRect.width - 40, 200))

    if fm1.enabled:
        text = basicFont.render(fm1.getFormattedTotalPour(), True, const.WHITE, const.BLACK)
        textRect = text.get_rect()
        windowSurface.blit(text, (windowInfo.current_w - textRect.width - 40, 200 + const.LINEHEIGHT))

    if fm2.enabled:
        text = basicFont.render(fm2.getFormattedTotalPour(), True, const.WHITE, const.BLACK)
        textRect = text.get_rect()
        windowSurface.blit(text, (windowInfo.current_w - textRect.width - 40, 200 + (2 * (const.LINEHEIGHT))))

    if view_mode == const.VIEW_MODE_TWEET:
        windowSurface.blit(tweet_bg, (0, 0))
        textRect = Rect(770, 100, 430, 470)
        drawText(windowSurface, tweet, const.BLACK, textRect, basicFont, True, None)

    if view_mode == const.VIEW_MODE_THIEF:
        windowSurface.blit(thief_bg, (0, 0))
        textRect = Rect(770, 100, 430, 470)
        drawText(windowSurface, tweet, const.BLACK, textRect, basicFont, True, None)

    # Display everything
    pygame.display.flip()


# number test
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


# check what is on taps
def check_tap(number):
    query = ('SELECT * FROM Keg WHERE id = (select currentKegId from Tap where id = ' + str(number) + ')')
    cnx.execute(query)
    row = cnx.fetchone()
    # set variables when keg found / or not
    if row:
        return row['name']
    else:
        return 'ERROR'


# check ABV on tap
def checkTapABV(number):
    query = ('SELECT * FROM Keg WHERE id = (select currentKegId from Tap where id = ' + str(number) + ')')
    cnx.execute(query)
    row = cnx.fetchone()
    # set variables when keg found / or not
    if row:
        if row['ABV']:
            return float(row['ABV'])
        else:
            return 0.00
    else:
        return 0.00


def checkTapTotalPour(number):
    query = ('SELECT sum(amount) as TotalPoured FROM Pour WHERE keg_id = ( select currentKegId from Tap where id = ' + str(number) + ')')
    cnx.execute(query)
    row = cnx.fetchone()
    if row:
        if row['TotalPoured']:
            return float(row['TotalPoured'])
        else:
            return 0
    else:
        return 0


def getCurrentKegId(tapNumber):
    query = ('SELECT * FROM Keg WHERE id = ( select currentKegId from Tap where id = ' + str(tapNumber) + ')')
    cnx.execute(query)
    row = cnx.fetchone()
    if row:
        return row['id']
    else:
        return 'Error'


def change_keg(tapNumber, newKegName, newKegABV):
    currentKegId = 0
    changeTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    currentKegId = getCurrentKegId(tapNumber)
    # end current keg serving time
    query = ("""Update Keg Set available_to = '""" + changeTime + """' where id = """ + str(currentKegId) + ";")
    cnx.execute(query)
    # insert new keg
    args = [newKegName, newKegABV, changeTime, tapNumber]
    query = 'insert into Keg (name, ABV, available_from, TapId) values (%s,%s,%s,%s)'
    cnx.execute(query, args)
    # get tap max id
    query = 'SELECT max(id) from Keg where TapId = %s;'
    args = [tapNumber, ]
    cnx.execute(query, args)
    row = cnx.fetchone()
    # print row['id']
    if row:
        currentKegId = int(row['max(id)'])
    else:
        print('ERROR')
    query = 'update Tap set currentKegId = %s where id = %s'
    args2 = [str(currentKegId), tapNumber]
    cnx.execute(query, args2)


def createDrinker():
    inputmode = True
    RFID = inputbox.ask(windowSurface, 'Drinkers RFID tag number')
    # RFID = str(inputbox.ask(windowSurface, 'Drinkers RFID tag number'))
    inputmode = False
    if RFID is not None:
        query = 'SELECT * from Drinker where rfid = %s;'
        args = [RFID, ]
        cnx.execute(query, args)
        row = cnx.fetchone()
        if row:
            return 'ERROR. RFID tag already in use!'
        else:
            inputmode = True
            name = inputbox.ask(windowSurface, 'Drinker name')
            inputmode = False
            if name is not None:
                inputmode = True
                weight = inputbox.askNumber(windowSurface, 'Drinker weight (kg)')
                inputmode = False
                if weight is not None:
                    args = [RFID, name, weight]
                    query = 'insert into Drinker (rfid, name, weight) values (%s,%s,%s)'
                    cnx.execute(query, args)
                    return f'Pleasure to beer you, {name}... You {random.choice(const.ADJECTIVES)}, {weight} kg {random.choice(const.PORTIONS)} of {random.choice(const.CALLNAMES)}!'
    return None


def addManualDrink():
    drinkTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    inputmode = True
    RFID = inputbox.ask(windowSurface, 'Read Drinker RFID tag')
    inputmode = False
    if RFID is not None:
        inputmode = True
        servings = inputbox.askNumber(windowSurface, 'Insert number of alcohol servings consumed (e.g. 1.5)')
        inputmode = False
        if servings is not None:
            args = [RFID, ]
            query = 'SELECT * from Drinker where rfid = %s;'
            cnx.execute(query, args)
            row = cnx.fetchone()
            if row:
                drinkerName = (row['name'])
                drinkerId = (row['id'])
                args = [drinkerId, drinkTime, servings]
                query = 'insert into Serving (drinker_id, drank_at, servings) values (%s,%s,%s)'
                cnx.execute(query, args)
                #ac.buildRecordAndTrySend('Serving', cnx.cursor.lastrowid)
                text_to_blit = f'{drinkerName} just drank {servings} alcohol servings'
                return text_to_blit
            else:
                return 'ERROR. Drinker not found!'
    return None


def getDrinkerBAC(drinkerId):
    temp = drinkerId
    args = [drinkerId, temp]
    query = 'SELECT BAC from BAC where drinker_id = %s and id = (select max(id) from BAC where drinker_id = %s)'
    cnx.execute(query, args)
    row = cnx.fetchone()
    if row:
        return float(row['BAC'])
    else:
        return 0.0


def getDrinkerTotalPoured(drinkerId):
    args = [drinkerId, ]
    query = 'SELECT sum(amount) as TotalPoured FROM Pour WHERE user_id = %s and poured_at > ' + const.POUR_CUTOFF
    cnx.execute(query, args)
    row = cnx.fetchone()
    if row:
        if row['TotalPoured']:
            return float(row['TotalPoured'])
        else:
            return 0
    else:
        return 0


def editLastPour():
    return None
    # Check what was poured last
    query = ('SELECT * FROM Pour WHERE keg_id = ( select currentKegId from Tap where id = ' + str(number) + ')')
    cnx.execute(query)
    row = cnx.fetchone()
    if row:
        if row['TotalPoured']:
            return float(row['TotalPoured'])
        else:
            return 0
    else:
        return 0
    drinkTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    inputmode = True  # TODO: Why?
    RFID = str(inputbox.ask(windowSurface, 'Read Drinker RFID tag'))
    servings = str(inputbox.askNumber(windowSurface, 'Insert number of alcohol servings consumed (eg. 1.5)'))
    inputmode = False

    args = [RFID, ]
    query = 'SELECT * from Drinker where rfid = %s;'
    cnx.execute(query, args)
    row = cnx.fetchone()
    if row:
        drinkerName = (row['name'])
        drinkerId = (row['id'])
        args = [drinkerId, drinkTime, servings]
        query = 'insert into Serving (drinker_id, drank_at, servings) values (%s,%s,%s)'
        cnx.execute(query, args)
        text_to_blit = f'{drinkerName} just drank {servings} alcohol servings'
        return text_to_blit
    else:
        return 'ERROR. Drinker not found!'


# Tap1, on Pin
def doAClick(channel):
    currentTime = int(time.time() * FlowMeter.MS_IN_A_SECOND)
    if fm1.enabled:
        fm1.update(currentTime)
        # pd.leds[6].toggle()


# Tap2, on Pin
def doAClick2(channel):
    currentTime = int(time.time() * FlowMeter.MS_IN_A_SECOND)
    if fm2.enabled:
        fm2.update(currentTime)
        # pd.leds[7].toggle()


def resetDrinker(self):
    print('reseted')
    currentDrinkerId = 1
    currentDrinkerName = 'NO USER'


listener.register(0, pifacedigitalio.IODIR_RISING_EDGE, doAClick)
listener.register(1, pifacedigitalio.IODIR_RISING_EDGE, doAClick2)
listener.activate()

# main loop
quit = False
while not quit:
    if (taps_checked is False):
        currentTap1 = check_tap(1)
        currentTap2 = check_tap(2)
        fm1.setTotalPour(checkTapTotalPour(1))
        fm2.setTotalPour(checkTapTotalPour(2))
        ABV1 = checkTapABV(1)
        ABV2 = checkTapABV(2)
        ABVs = [ABV1, ABV2]
        taps_checked = True

    # Handle keyboard events
    for event in pygame.event.get():
        if event.type == QUIT or (event.type == KEYUP and event.key == K_F10):
            quit = True
        elif event.type == KEYUP and event.key == K_q:
            fm1.enabled = not(fm1.enabled)
        elif event.type == KEYUP and event.key == K_w:
            fm2.enabled = not(fm2.enabled)
        elif event.type == KEYUP and event.key == K_d:  # (d) New Drinker
            text_for_print = createDrinker()
            if text_for_print is not None:
                view_mode = const.VIEW_MODE_TWEET
                lastTweet = int(time.time() * FlowMeter.MS_IN_A_SECOND)
            text_buffer = ''
        elif event.type == KEYUP and event.key == K_c:  # (c) Manual Drink
            text_for_print = addManualDrink()
            if text_for_print is not None:
                view_mode = const.VIEW_MODE_TWEET
                lastTweet = int(time.time() * FlowMeter.MS_IN_A_SECOND)
            text_buffer = ''
        elif event.type == KEYUP and event.key == K_n:  # (n) Change Tap 1
            inputmode = True
            newKegName = inputbox.ask(windowSurface, 'Type in the product now on Tap 1. DO NOT include ABV (X.XX%)!')
            inputmode = False
            if newKegName is not None:
                inputmode = True
                newKegABV = inputbox.askNumber(windowSurface, 'Type in the ABV (X.XX%)')
                inputmode = False
                if newKegABV is not None:
                    taps_checked = False
                    change_keg(1, newKegName, newKegABV)
            text_buffer = ''
        elif event.type == KEYUP and event.key == K_m:  # (m) Change Tap 2
            inputmode = True
            newKegName = inputbox.ask(windowSurface, 'Type in the product now on Tap 2. DO NOT include ABV (X.XX%)!')
            inputmode = False
            if newKegName is not None:
                inputmode = True
                newKegABV = inputbox.askNumber(windowSurface, 'Type in the ABV (X.XX%)')
                inputmode = False
                if newKegABV is not None:
                    taps_checked = False
                    change_keg(2, newKegName, newKegABV)
            text_buffer = ''
        elif event.type == KEYUP and event.key == K_z:  # (z) Edit last pour
            text_for_print = editLastPour()
            if text_for_print is not None:
                view_mode = const.VIEW_MODE_TWEET
                lastTweet = int(time.time() * FlowMeter.MS_IN_A_SECOND)
            text_buffer = ''
        elif event.type == KEYUP and event.key == K_s:  # (s) Check drinker status
            RFID = inputbox.ask(windowSurface, 'Read Drinker RFID tag')
            if RFID is not None:
                args = [RFID, ]
                query = 'SELECT * from Drinker where rfid = %s;'
                cnx.execute(query, args)
                row = cnx.fetchone()
                if row:
                    drinkerId = (row['id'])
                    currentBAC = getDrinkerBAC(drinkerId)
                    currentTotalPour = getDrinkerTotalPoured(drinkerId)
                    text_for_print = f'You have drank {str(currentTotalPour)} l of beer and are {str(currentBAC)} permille drunk.'
                    view_mode = const.VIEW_MODE_TWEET
                    lastTweet = int(time.time() * FlowMeter.MS_IN_A_SECOND)
            text_buffer = ''
        # ---------------------- TEST SETUP ------------------------------------
        elif event.type == KEYUP and event.key == K_u:  # (u) Add test drink
            currentTime = int(time.time() * FlowMeter.MS_IN_A_SECOND)
            if fm1.enabled:
                fm1.clicks += 0.5
                fm1.clickDelta = max((currentTime - fm1.lastClick), 1)
                if (fm1.enabled is True):
                    clickPour = 0.5
                    fm1.thisPour += clickPour
                    fm1.totalPour += clickPour
                fm1.lastClick = currentTime
        elif event.type == KEYUP and event.key == K_i:  # (i) Add test drink
            currentTime = int(time.time() * FlowMeter.MS_IN_A_SECOND)
            if fm2.enabled:
                fm2.clicks += 0.33
                fm2.clickDelta = max((currentTime - fm2.lastClick), 1)
                if (fm2.enabled is True):
                    clickPour = 0.33
                    fm2.thisPour += clickPour
                    fm2.totalPour += clickPour
                fm2.lastClick = currentTime

        elif event.type == KEYUP and event.key == K_o:  # (o) Add test drink
            for i in range(225):
                doAClick(1)
        elif event.type == KEYUP and event.key == K_p:  # (p) Add test drink
            for i in range(225):
                doAClick2(2)
        # /--------------------- TEST SETUP ------------------------------------
        elif event.type == KEYUP and inputmode is False and event.key == K_RETURN:
            if not is_number(text_buffer):
                text_buffer = ''
                break
            # Escape from tweet, if a tweet is being displayed
            view_mode = const.VIEW_MODE_NORMAL

            # Try to find user from SQL
            query = ('SELECT * FROM Drinker WHERE rfid = ' + text_buffer)
            cnx.execute(query)
            row = cnx.fetchone()

            # set variables when user found / or not
            if row:
                currentDrinkerName = row['name']
                currentDrinkerId = row['id']
                text_for_print = f'Kaljaa kurkusta alas, {currentDrinkerName}!'
            else:
                currentDrinkerName = const.NO_USER_DISPLAY_NAME
                currentDrinkerId = const.NO_USER_ID
                text_for_print = "User not found, please create a new user! (press 'd')"
                view_mode = const.VIEW_MODE_TWEET
                lastTweet = int(time.time() * FlowMeter.MS_IN_A_SECOND)

            text_buffer = ''
            lastDrinkerUpdate = int(time.time() * FlowMeter.MS_IN_A_SECOND)
        elif event.type == KEYUP and inputmode is False:  # record ALL key presses for RFID tagging
            text_buffer += pygame.key.name(event.key)

    currentTime = int(time.time() * FlowMeter.MS_IN_A_SECOND)
    # Pour inspection
    for ii, fm in enumerate(fms):
        tap_id = ii + 1  # Indexing starts from 1 for taps
        if (fm.thisPour > const.MIN_POUR_VOLUME_LITRE and currentTime - fm.lastClick > const.INACTIVITY_TIME_MILLISECOND):  # X seconds of inactivity causes a pour
            thisServing = fm.thisPour * ABVs[ii] * const.SERVINGS_PER_LITRE_AND_ABV_PERCENTAGE  # alcohol servings
            formattedThisServing = str(('{0:.2f}'.format(thisServing))) + ' servings'

            if currentDrinkerId == const.NO_USER_ID:
                view_mode = const.VIEW_MODE_THIEF
                text_for_print = const.THIEF_TWEET_TEXT
            else:
                view_mode = const.VIEW_MODE_TWEET
                text_for_print = f'{currentDrinkerName} just poured {fm.getFormattedThisPour()} ({formattedThisServing}) of {check_tap(tap_id)} from Hana {tap_id}!'

            lastTweet = int(time.time() * FlowMeter.MS_IN_A_SECOND)
            pourTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            pour_args = [currentDrinkerId, tap_id, getCurrentKegId(tap_id), pourTime, fm.thisPour]
            serving_args = [currentDrinkerId, pourTime, float(thisServing)]

            # Insert Pour to DB
            cnx.execute(pour_query, pour_args)

            # If not a thief-pour, try to send Pour to API
            #if currentDrinkerId != const.NO_USER_ID:
            #    try:
            #        ac.buildRecordAndTrySend('Pour', cnx.cursor.lastrowid)
            #    except Exception as e:
            #        print(e)

            # Serving insert to DB must occur after Pour has been sent to API, because of lastrowid above
            cnx.execute(serving_query, serving_args)

            # Reset variables after successful pour
            fm.thisPour = 0.0
            fm.thisServing = 0.0
            currentDrinkerId = const.NO_USER_ID
            currentDrinkerName = const.NO_USER_DISPLAY_NAME

    # check viewmode
    if currentTime - lastTweet > const.TWEET_SHOW_TIME_MILLISECOND:  # Tweet shows for X seconds, then resets
        view_mode = const.VIEW_MODE_NORMAL

    # reset drinker if nothing poured in X secs
    if (currentDrinkerId != const.NO_USER_ID and fm1.thisPour <= const.MIN_POUR_VOLUME_LITRE and fm2.thisPour <= const.MIN_POUR_VOLUME_LITRE and currentTime - lastDrinkerUpdate > const.RESET_DRINKER_TIME_MILLISECOND):
        fm1.thisPour = 0.0
        fm2.thisPour = 0.0
        currentDrinkerId = const.NO_USER_ID
        currentDrinkerName = const.NO_USER_DISPLAY_NAME

    # Update the screen
    if currentTime - lastRender > const.FRAME_RATE_MAX:
        renderThings(fm1, fm2, text_for_print, windowSurface, basicFont, currentDrinkerName)
        lastRender = int(time.time() * FlowMeter.MS_IN_A_SECOND)
try:
    cnx.close()
    pygame.quit()
    sys.exit()

except Exception as e:
    print(e)
    print('kill os  ' + str(datetime.datetime.now()))
    os._exit
