import pygame, sys
from pygame.locals import *

class adabot():
  image = ''
  x = 0
  y = 0
  ll = 0 # left limit
  rl = 0 # right limit
  direction = 'right'

  def __init__(self,image_file, x, y, ll, rl):
    self.image = pygame.image.load(image_file)
    self.image = self.image.convert_alpha()
    self.x = x
    self.y = y
    self.ll = ll
    self.rl = rl

  def update(self):
    if (self.direction == 'right'):
      self.x += 3
    else:
      self.x -= 3

    if (self.x > self.rl and self.direction != 'left'):
        self.direction = 'left'
        self.image = pygame.transform.flip(self.image, True, False)

    elif (self.x < self.ll and self.direction != 'right'):
        self.direction = 'right'
        self.image = pygame.transform.flip(self.image, True, False)
