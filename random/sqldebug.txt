in terminal start mysql w/ user ctkegbot, IDENTIFIED password and use db ctkegger:
> mysql -u ctkegbot -pKALJAAkurkusta ctkegger

in mysql terminal, run query:

SELECT d.name AS drinkerName,
      CAST(p.amount AS CHAR) AS consumedQty,
      k.name AS beerDescription,
      p.poured_at,
      DATE_FORMAT(p.poured_at, '%d-%m-%Y') AS date,
      DATE_FORMAT(p.poured_at, '%Y-%m-%dT%TZ') AS dateTime
FROM Pour p
  INNER JOIN Drinker d  ON p.user_id = d.id
    INNER JOIN Keg k ON p.keg_id = k.id
          WHERE p.id = (select id from pour limit 1)
