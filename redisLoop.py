# -*- coding: iso-8859-1 -*-
import sys
import schedule
import time
import datetime
import json
import redis
import requests
import utils.constants as const
import utils.apiCalls as ac
print('started redisLoop.py.')
sys.stdout.flush()
# set up REDIS
redisServer = redis.Redis(host='localhost', port=6379, db=0)


def popFromList(redisList):
    data = redisServer.lpop(redisList)
    if data:
        jsonData = json.loads(data)
        print(f'Popped jsonData from REDIS: {jsonData}')
        sys.stdout.flush()
        return ac.postJsonToAPI(jsonData, False, redisList)
    else:
        return True


def checkRedisLists():
    poursOk = redisServer.llen('pours') == 0
    servingsOk = redisServer.llen('servings') == 0
    bacsOk = redisServer.llen('bacs') == 0
    loopCount = 0

    if poursOk and servingsOk and bacsOk:
        print(f"{datetime.datetime.now().strftime('%H:%M:%S')} - REDIS empty, nothing to do!")
        sys.stdout.flush()
    else:
        try:
            redisServer.ping()
        except Exception as e:
            print(f'Redis server not on! Run command `redis-server` in a separate window. e: {e}')
            sys.stdout.flush()
        else:
            while loopCount < const.REDIS_LOOP_LIMIT:
                if (redisServer.llen('pours') == 0 and
                    redisServer.llen('servings') == 0 and
                    redisServer.llen('bacs') == 0):
                    break
                loopCount += 1
                print(f'Loop count: {loopCount}')
                if (not popFromList('pours') or
                    not popFromList('servings') or
                    not popFromList('bacs')):
                    print(f'API not responding.')
                    sys.stdout.flush()
                    break


schedule.every().minute.do(checkRedisLists)

while 1:
    schedule.run_pending()
    time.sleep(1)
