#!/bin/bash
trap "exit" INT TERM ERR
trap "kill 0" EXIT

LOGPATH="./logs"
STARTLOG="start.log"
REDISLOG="redis-server.log"
#declare apps which will be started.
#Last one will be in foreground, others will start in background
#declare python apps for start
declare -a APPS=("python3 bacUpdate.py" "python3 ctkegger_bot.py" "python3 kegomatic.py")
#keep the order as the same as above
declare -a LOGFILES=("bacUpdate.log" "zeroPourChecker.log" "redisLoop.log" "ctkegger_bot.log" "kegomatic.log")

echo $(date -u) "INFO: Starting kegomatic"  | tee -a $LOGPATH/$STARTLOG

#check redis & start if not running
if [[ "$( echo 'ping' | redis-cli )" == "PONG" ]] ; then
    echo $(date -u) "INFO: REDIS is already running. no action needed"  | tee -a $LOGPATH/$STARTLOG
else
    echo $(date -u) "WARNING: STARTING REDIS. would be wise to start this otherwise"  | tee -a $LOGPATH/$STARTLOG
    redis-server 2>&1 | tee -a $LOGPATH/$REDISLOG &
    echo $(date -u) "INFO: REDIS started. Giving 5 seconds for redis to start."  | tee -a $LOGPATH/$STARTLOG
    sleep 3
fi

#check mysql and attempt to start is not running
UP=$(pgrep mysql | wc -l);
if [ "$UP" -ne 1 ];
then
        echo $(date -u) "WARNING: MYSQL is not running. would be wise to start this otherwise"  | tee -a $LOGPATH/$STARTLOG
        sudo service mysql start

else
        echo $(date -u) "INFO: MYSQL is already running. no action needed"  | tee -a $LOGPATH/$STARTLOG
fi


for i in "${!APPS[@]}"; do #handle only the index due to use of "!"
  APP="${APPS[$i]}" #helper to store app via index
  LOG="$LOGPATH/${LOGFILES[$i]}" #helper for log path via index
  echo $(date -u) "INFO: Starting $APP and pushing prints + logs to $LOG"  | tee -a $LOGPATH/$STARTLOG
  $APP 2>&1 | awk '{ print strftime("%c: "), $0; fflush(); }' | tee -a $LOG &
done
echo $(date -u) "INFO: POURING BEER AND ENJOYING LIFE."  | tee -a $LOGPATH/$STARTLOG


wait
