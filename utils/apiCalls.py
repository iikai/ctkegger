import json
import redis
import requests
import utils.constants as const
import utils.sql_queries as sq
from utils.dbConnection import Database

# set up REDIS
redisServer = redis.Redis(host='localhost', port=6379, db=0)


def postJsonToAPI(jsonData, newRecord, redisList):
    try:  # We try to send the record to the API endpoint
        if redisList == 'bacs':
            url = const.BAC_URL
        else:
            url = const.DRINK_URL
        print(f'Sending JSON Data: {jsonData}')
        response = requests.post(url, json=jsonData, headers=const.API_HEADERS)
    except Exception as e:
        print(f'Endpoint call failed! e: {e}')
        # If API call fails, record is saved back to the REDIS queue to wait for resend.
        # Also causes further calls to be put in queue as long as REDIS is handled.
        try:
            print(f'Pushing data to REDIS due to an Exception in API call: {jsonData}')
            # New record is pushed at the back of the queue and old back to the front from where it was taken
            if newRecord:
                redisServer.rpush(redisList, json.dumps(jsonData))
            else:
                redisServer.lpush(redisList, json.dumps(jsonData))
        except Exception as e:
            print(f'Redis server not on! Run command `redis-server` in a separate window. e: {e}')
            return False
        else:
            return False
    else:
        # Same as with Exception in the call but now the call does not raise an exception but fails peacefully
        # Some other that 200 may be handled as OK and may need to be added.
        if response.status_code != 200:
            try:
                print(f'Pushing data to REDIS due to status_code ({response.status_code}): {jsonData}')
                # New record is pushed at the back of the queue and old back to the front from where it was taken
                if newRecord:
                    redisServer.rpush(redisList, json.dumps(jsonData))
                else:
                    redisServer.lpush(redisList, json.dumps(jsonData))
            except Exception as e:
                print(f'Redis server not on! Run command `redis-server` in a separate window. e: {e}')
                return False
            else:
                return False
        else:
            print(f'Send status_code ({response.status_code}) was OK!')
            return True


def buildRecordAndTrySend(table, id):
    if table == 'Pour':
        record = sq.get_pour_for_API(id)
        redisList = 'pours'
    else:
        record = sq.get_serving_for_API(id)
        redisList = 'servings'

    jsonData = [record]
    if redisServer.llen(redisList) == 0:  # There is no queue in REDIS, all good
        postJsonToAPI(jsonData, True, redisList)
    else:  # There is a queue in REDIS -> Get at the back of the queue
        try:
            print(f'Pushing data to REDIS due to a queue in REDIS (length: {redisServer.llen(redisList)}): {jsonData}')
            redisServer.rpush(redisList, json.dumps(jsonData))
        except Exception as e:
            print(f'Redis server not on! Run command `redis-server` in a separate window. e: {e}')


def commonBacQuery(drinker_id, url):
    drinker = sq.drinker_by_ID(drinker_id)
    jsonData = { "drinkerName": drinker['name'] }
    print(f'Sending JSON Data for commonBacQuery: {jsonData}')
    response = requests.post(url, json=jsonData, headers=const.API_HEADERS)
    if response.status_code == 200:
        print(f'Send status_code ({response.status_code}) was OK!')
        bac_data = json.loads(response.text)
        if len(bac_data) == 1:
            bac_data[0]['name'] = drinker['name']
            return bac_data[0]
        else:
            return { 'name': drinker['name'] }
    else:
        print(f'Failed with status_code: {response.status_code}: {jsonData}')
        return { 'error': f'Error: {response.status_code}' }
