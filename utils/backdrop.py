import pygame
import random


class Backdrop():
    def __init__(self, image_file):
        self.image_file = image_file
        self.image = pygame.image.load(random.choice(image_file))
        self.image = self.image.convert_alpha()
        self.y_min = 2
        self.y_max = 500
        self.y = random.randint(self.y_min, self.y_max)
        self.ll = -self.image.get_size()[0]
        self.rl = 1280 + self.image.get_size()[0]
        self.direction = random.choice(['left', 'right'])
        if self.direction == 'right':
            self.x = self.ll
        else:
            self.x = self.rl
        self.speed_min = 2
        self.speed_max = 20
        self.speed = random.randint(self.speed_min, self.speed_max)

    def update(self):
        if (self.direction == 'right'):
            self.x += self.speed
        else:
            self.x -= self.speed

        if (self.x < self.ll or self.x > self.rl):
            self.image = pygame.image.load(random.choice(self.image_file))
            self.image = self.image.convert_alpha()
            self.ll = -self.image.get_size()[0]
            self.rl = 1280 + self.image.get_size()[0]
            if random.choice([True, False]):
                self.image = pygame.transform.flip(self.image, True, False)
            self.direction = random.choice(['left', 'right'])
            if self.direction == 'right':
                self.x = self.ll
            self.y = random.randint(self.y_min, self.y_max)
            self.speed = random.randint(self.speed_min, self.speed_max)
