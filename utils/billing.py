import pandas as pd
import numpy as np
from pathlib import Path

dir_dump = 'csv_dumps/CT21'
# dir_dump = 'ct20_dumps'
filename_result = 'Billing_for_pours.csv'
save_results_to_file = True
calibration_user = 'VARAS'

# Parameters for hero kegs (i.e., kegs that still had beer in them after the event)
hero_ids = [23, 24]
hero_amount_totals = [30, 30]
hero_charge_for_surplus = [0, 1]
# df.loc[df['keg_id'] == 24, 'pour_amount'].sum()

# ToDo: separate costs by who paid for the keg
# # Parameters for billing groups
# groups = [[20], [24]]
# num_groups = len(groups)

path_dump = Path(__file__).resolve().parents[1] / 'db' / dir_dump

# Load data
df_drinkers = pd.read_csv(path_dump / 'Drinkers.csv')
df_pours = pd.read_csv(path_dump / 'Pours.csv')
df_kegs = pd.read_csv(path_dump / 'Kegs.csv')
df_keg_prices = pd.read_csv(path_dump / 'Keg_prices.csv')

# Rename dumb columns
df_drinkers.rename(columns={'id': 'user_id', 'name': 'user_name'}, inplace=True)
df_kegs.rename(columns={'id': 'keg_id', 'name': 'keg_name'}, inplace=True)
df_keg_prices.rename(columns={'id': 'keg_id', 'name': 'keg_name', 'price': 'price_per_keg'}, inplace=True)
df_pours.rename(columns={'id': 'pour_id', 'amount': 'pour_amount'}, inplace=True)

# Drop unnecessary columns
df_drinkers.drop(columns=['weight', 'rfid'], inplace=True)
df_kegs.drop(columns=['available_from', 'available_to', 'TapId', 'ABV'], inplace=True)
df_pours.drop(columns=['pour_id', 'tap_id', 'poured_at'], inplace=True)

# Sum of pours for each user, per keg_id
df_amount_per_user_id_and_keg_id = df_pours.groupby(['user_id', 'keg_id'])[['pour_amount']].sum().reset_index()

# Sum of pours for each user
df_amount_per_user_id = df_pours.groupby(['user_id'])[['pour_amount']].sum().reset_index()

# Total amount poured for each keg
df_amount_per_keg_id = df_pours.groupby(['keg_id'])[['pour_amount']].sum().reset_index()
df_amount_per_keg_id.rename(columns={'pour_amount': 'total_amount'}, inplace=True)

# Take not fully-consumed kegs into account. Essentially save relevant data into variables, then replace total
# poured amount with the volume of the whole keg. The point here is this: if actual pour amount for last keg is used,
# user will be billed too high a price per amount. Instead, we want to distribute the cost of the unconsumed portion of
# the last keg between all users.
hero_prices = []
hero_amount_poureds = []
hero_unconsumed_prices = []
for hh, hero_id in enumerate(hero_ids):
    # Hero price and pour amount
    hero_prices.append(float(df_keg_prices.loc[df_keg_prices['keg_id'] == hero_id].price_per_keg))
    hero_amount_poureds.append(
        df_amount_per_keg_id['total_amount'].loc[df_amount_per_keg_id['keg_id'] == hero_id].item())

    # Update total amount for hero
    df_amount_per_keg_id.loc[df_amount_per_keg_id.index[df_amount_per_keg_id['keg_id'] == hero_id], 'total_amount'] = \
        hero_amount_totals[hh]

    # Adjusted price for hero keg
    hero_unconsumed_amount = hero_amount_totals[hh] - hero_amount_poureds[hh]
    hero_unconsumed_portion = hero_unconsumed_amount / hero_amount_totals[hh]
    hero_unconsumed_prices.append(hero_prices[hh] * hero_unconsumed_portion)

# Build data frame with almost all the data. Start by combining keg prices and keg amounts
df = pd.merge(df_keg_prices, df_amount_per_keg_id, on='keg_id')

# Price per amount
df['price_per_amount'] = df.apply(lambda row: row.price_per_keg / row.total_amount, axis=1)

# Per-keg pour amounts for users
df = pd.merge(df, df_amount_per_user_id_and_keg_id, on='keg_id')

# Calculate price for each per-keg pour amount for users
df['price'] = df.apply(lambda row: row.pour_amount * row.price_per_amount, axis=1)

# Sum of per-keg pour amount prices
df_result = df.groupby(['user_id'])[['price']].sum().reset_index()

# Include drinker names and total pour amounts per user to result
df_result = pd.merge(df_drinkers, df_result, on='user_id')
df_result = pd.merge(df_result, df_amount_per_user_id, on='user_id')

# Remove calibration user and distribute its cost to all users
calibration_user_price = float(df_result.loc[df_result['user_name'] == calibration_user].price)
df_result.drop(df_result[df_result.user_name == calibration_user].index, inplace=True)
num_users = len(df_result)
df_result['price'] = df_result['price'].apply(lambda row: row + calibration_user_price / num_users)

# ToDo: distribute unconsumed beer weigthed by total amount poured per user
# Distribute unconsumed beer cost to all users
for hh, hero_id in enumerate(hero_ids):
    if hero_charge_for_surplus[hh]:
        df_result['price'] = df_result['price'].apply(lambda row: row + hero_unconsumed_prices[hh] / num_users)

# Checksums
user_prices_sum_total = df_result['price'].sum()
keg_prices_sum_total = df_keg_prices['price_per_keg'].sum()
for hh, charge in enumerate(hero_charge_for_surplus):  # Take hero surpluses into account
    if not charge:
        keg_prices_sum_total = keg_prices_sum_total - hero_unconsumed_prices[hh]
if np.abs(user_prices_sum_total - keg_prices_sum_total) > 0.5:
    print('WARNING: CHECKSUM FAILURE')

# Sort results by descending pour amount
df_result.sort_values('pour_amount', ascending=False, inplace=True)

# Save results
if save_results_to_file:
    df_result.to_csv(path_dump / filename_result, index=False)
