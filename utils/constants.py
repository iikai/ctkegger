# MYSQL
DATABASE_NAME = 'ctkegger'

# COSMETICS
VIEW_WIDTH = 1280
VIEW_HEIGHT = 720
FRAME_RATE_MAX = 50  # 20 Hz = 1 / 50 ms
FONTNAME = 'assets/fonts/prstartk.ttf'
FONTSIZE = 20
SMALLFONTSIZE = 12
LARGEFONTSIZE = 40
LINEHEIGHT = 28
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# IMAGES
BG_DEFAULT = 'assets/images/ct19-bg.png'
BG_TWEET = 'assets/images/ct19-tweet.png'
BG_THIEF = 'assets/images/thief-bg.png'
BACKDROPS = [
    'assets/images/cucumber.png',
    'assets/images/karjala.png',
    'assets/images/elvis.png'
]

# TEMPORAL
INACTIVITY_TIME_MILLISECOND = 8000
TWEET_SHOW_TIME_MILLISECOND = 5000
RESET_DRINKER_TIME_MILLISECOND = 30000
POUR_CUTOFF = '2020-01-01'

# DISPLAY TEXTS
ADJECTIVES = ['lovely', 'juicy', 'sexy', 'bodacious', 'fluffy', 'tasty', 'lucious']
PORTIONS = ['piece', 'slice', 'hunk', 'pile']
CALLNAMES = ['ass', 'muscle', 'meat', 'lard', 'scrotum']
NO_USER_DISPLAY_NAME = 'NO USER'
TAP_NOT_UPDATED_DISPLAY_NAME = 'TAP NOT UPDATED!'
KEY_EXPLANATIONS = '(c) Manual Drink, (d) New Drinker, (s) Check stats, (n) Change Tap 1, (m) Change Tap 2'
HAIL_TEXT_FOR_LOGGED_IN_USER = 'Kaaleppia hanasta tuoppiin, se!'
THIEF_TWEET_TEXT = 'PERKELE!!! Someone just poured beer WITHOUT LOGGING IN! Please use Telegram bot to mark the pour in your name.'

# MISC
NO_USER_ID = 1
SERVINGS_PER_LITRE_AND_ABV_PERCENTAGE = 1 / 100.0 * 789.24 / 12.0
FLOW_METER_CORRECTION_FACTORS = [1/0.86911927*0.9, 1/0.94901608*1.1*1.15]

# BAC
BAC_MAX_INTERVAL_DAY = 2
BAC_TIME_STEP_MINUTE = 1
BAC_SPAN_SERVING_TIME_STEPS = 30
BAC_UPDATE_INTERVAL_MINUTE = 5

# API
BAC_URL = 'https://prod-141.westeurope.logic.azure.com/workflows/75618277d593405896490f24a8211fe5/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=IMl5cqsoHVIz3Ne8rz_Y7ls8B-LtrzSCbwWRb1BWnDQ'
DRINK_URL = "https://prod-129.westeurope.logic.azure.com/workflows/2b1984244f42408698c96f12784e811d/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=mKKGhpwi15GN34TbqV4sRYT_vEJIdtDKMfbxEP89bEs"
BAC_NOW_URL = 'https://prod-184.westeurope.logic.azure.com:443/workflows/e5e0a8b0b2b44f54b0cbf1939c912814/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=VOUCwL8P1K1vNWomCDhaTSaeoXeE1M0Zaqjvnc9epNI'
BAC_YESTERDAY_URL = 'https://prod-169.westeurope.logic.azure.com:443/workflows/11f4650e4e0f44dda282bd0fd1675519/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=M6pDYOBbq-4rV4IHba0ircQjdP7nwkTg9MOzYSN-cMQ'
API_HEADERS = {'content-type': 'application/json'}

# REDIS
REDIS_LOOP_LIMIT = 30
MIN_POUR_VOLUME_LITRE = 0.01

# VIEWMODES
VIEW_MODE_NORMAL = 'normal'
VIEW_MODE_THIEF = 'thief'
VIEW_MODE_TWEET = 'tweet'

# TELEGRAM BOT
TELEGRAM_BOT_ID = '1392074864:AAHl2hzNbqLLhAg3KytsmgtarkFn77027Cg'
TELEGRAM_DEVELOPER_CHAT_ID = '-1001479746897'
TELEGRAM_MAX_MESSAGE_LENGTH = 4096

# HTML
HTML_PERMILLE = '&#8240;'
