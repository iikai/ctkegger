#!/usr/bin/python3
import mysql.connector
from mysql.connector import errorcode


class Database():
    def __init__(self, name):
        try:
            self._conn = mysql.connector.connect(user='ctkegbot',
                                                 password='KALJAAkurkusta',
                                                 host='127.0.0.1',
                                                 database=name)
            self._cursor = self._conn.cursor(dictionary=True)
            self._conn.autocommit = True
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print('Something is wrong with your username or password')
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print('Database does not exist')
            else:
                print(err)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    @property
    def connection(self):
        return self._conn

    @property
    def cursor(self):
        return self._cursor

    def commit(self):
        self.connection.commit()

    def close(self, commit=True):
        if commit:
            self.commit()
        self.connection.close()

    def execute(self, sql, params=None):
        self.cursor.execute(sql, params or ())

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def rowcount(self):
        return self.cursor.rowcount()

    def lastrowid(self):
        return self.cursor.lastrowid()

    def query(self, sql, params=None):
        self.cursor.execute(sql, params or ())
        return self.fetchall()
