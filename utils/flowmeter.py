import time


class FlowMeter():
    SECONDS_IN_A_MINUTE = 60
    MS_IN_A_SECOND = 1000.0000
    beverage = 'beer'
    enabled = True
    clicks = 0
    lastClick = 0
    clickDelta = 0
    thisPour = 0.0000  # in Liters
    totalPour = 0.0000  # in Liters

    def __init__(self, beverage, correction_factor=1.):
        self.beverage = beverage
        self.clicks = 0
        self.lastClick = int(time.time() * FlowMeter.MS_IN_A_SECOND)
        self.clickDelta = 0
        self.thisPour = 0.0000
        self.totalPour = 0.0000
        self.thisServing = 0.0000
        self.enabled = True
        self.correction_factor = correction_factor

    def update(self, currentTime):
        self.clicks += 1
        # get the time delta
        self.clickDelta = max((currentTime - self.lastClick), 1)
        # add standard volume associated with each click
        if (self.enabled is True and self.clickDelta < 1000):
            clickPour = 1 / 450  # liters per click
            clickPour = self.correction_factor * clickPour  # Apply correction factor
            self.thisPour += clickPour
            self.totalPour += clickPour
        # Update the last click
        self.lastClick = currentTime

    def getFormattedClickDelta(self):
        return str(self.clickDelta) + ' ms'

    def getFormattedThisPour(self):
        return str(("{0:.2f}".format(round(self.thisPour, 3)))) + ' L'

    def getFormattedTotalPour(self):
        return str(("{0:.2f}".format(round(self.totalPour, 3)))) + ' L'

    def setTotalPour(self, totalPourSQL):
        self.totalPour = totalPourSQL

    def clear(self):
        self.thisPour = 0
        self.totalPour = 0
        self.thisServing = 0
