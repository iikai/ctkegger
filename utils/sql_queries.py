# -*- coding: iso-8859-1 -*-
import datetime
import utils.constants as const
from utils.dbConnection import Database

# set up database
cnx_sql = Database(const.DATABASE_NAME)

def get_pour_for_API(id):
    query = ('SELECT '
               'd.name AS drinkerName, '
               'CAST(p.amount AS CHAR) AS consumedQty, '
               'k.name AS beerDescription, '
               "DATE_FORMAT(p.poured_at, '%Y-%m-%dT00:00:00Z') AS date, "
               "DATE_FORMAT(p.poured_at, '%Y-%m-%dT%TZ') AS dateTime "
           'FROM Pour p '
           'INNER JOIN Drinker d '
               'ON p.user_id = d.id '
           'INNER JOIN Keg k '
               'ON p.keg_id = k.id '
           f'WHERE p.id = {id};')
    cnx_sql.execute(query)
    record = cnx_sql.fetchone()
    record['lineType'] = 'Pour'
    return record


def get_serving_for_API(id):
    query = ('SELECT '
               'd.name AS drinkerName, '
               'CAST(s.servings AS CHAR) AS consumedQty, '
               "DATE_FORMAT(s.drank_at, '%Y-%m-%dT00:00:00Z') AS date, "
               "DATE_FORMAT(s.drank_at, '%Y-%m-%dT%TZ') AS dateTime "
           'FROM Serving s '
           'INNER JOIN Drinker d '
               'ON s.drinker_id = d.id '
           f'WHERE s.id = {id};')
    cnx_sql.execute(query)
    record = cnx_sql.fetchone()
    record['lineType'] = 'Serving'
    record['beerDescription'] = ''
    return record


def get_BAC_for_API(id):
    query = ('SELECT '
               'd.name AS drinkerName, '
               'CAST(b.BAC AS CHAR) AS bacValue, '
               "DATE_FORMAT(b.time, '%Y-%m-%dT%TZ') AS dateTime "
           'FROM BAC b '
           'INNER JOIN Drinker d '
               'ON b.drinker_id = d.id '
           f'WHERE b.id = {id};')
    cnx_sql.execute(query)
    record = cnx_sql.fetchone()
    return record

def list_all_drinkers():
    query = ('SELECT id, name FROM Drinker')
    cnx_sql.execute(query)
    return cnx_sql.fetchall()


def list_recent_drinkers():
    time_now = datetime.datetime.now()
    current_time = time_now.strftime('%Y-%m-%d %H:%M:00')
    query = ('SELECT id, name '
           'FROM Drinker '
           f"WHERE id IN (SELECT distinct drinker_id from Serving where drank_at > '{current_time}' - INTERVAL {const.BAC_MAX_INTERVAL_DAY} DAY) AND id > {const.NO_USER_ID};")
    cnx_sql.execute(query)
    return cnx_sql.fetchall()


def drinker_by_ID(id):
    query = f'SELECT * FROM Drinker WHERE id = {id};'
    cnx_sql.execute(query)
    return cnx_sql.fetchone()


def list_thief_pours():
    query = 'SELECT ' \
            'p.keg_id, ' \
            'p.id AS pour_id, ' \
            'p.tap_id AS tap_id, ' \
            'k.name AS beer_name, ' \
            'p.poured_at AS poured_at, ' \
            'p.amount AS amount ' \
            'FROM Pour p ' \
            'JOIN Keg k ON p.keg_id = k.id ' \
            f'WHERE p.user_id = {const.NO_USER_ID}'
    cnx_sql.execute(query)
    return cnx_sql.fetchall()


def whats_in_taps():
    query = 'SELECT ' \
            't.id AS tap_id, ' \
            't.currentKegID, ' \
            'k.name AS beer_name, ' \
            'k.available_from AS available_from, ' \
            'k.ABV AS abv, ' \
            'COALESCE( (SELECT SUM(amount) FROM Pour p WHERE p.keg_id = t.currentKegID), 0) AS total_poured ' \
            'FROM Tap t ' \
            'JOIN Keg k ' \
            'ON t.currentKegId = k.id ' \
            'ORDER BY t.id ASC'
    cnx_sql.execute(query)
    return cnx_sql.fetchall()

def latest_bac_by_drinker(drinker_id):
    query = 'SELECT C.id, C.name AS Name, A.BAC*1.18/0.83 AS BAC ' \
            'FROM BAC A ' \
            'INNER JOIN (' \
            '    SELECT drinker_id, MAX(time) time ' \
            '    FROM BAC ' \
            '    GROUP BY drinker_id' \
            ') B ON A.drinker_id = B.drinker_id AND A.time = B.time ' \
            'INNER JOIN Drinker C ON C.id = A.drinker_id ' \
            f'WHERE C.id={drinker_id} ' \
            'ORDER BY A.BAC DESC'
    cnx_sql.execute(query)
    return cnx_sql.fetchone()

def yesterdays_latest_bac_by_drinker(drinker_id):
    query = 'SELECT B.name AS Name, MAX(A.BAC)*1.18/0.83 BAC ' \
            'FROM BAC A ' \
            'INNER JOIN Drinker B ON B.id=A.drinker_id ' \
            f'WHERE A.time >= CURRENT_DATE - INTERVAL 1 DAY AND A.time < CURRENT_DATE AND A.drinker_id = {drinker_id} ' \
            'GROUP BY A.drinker_id'
    cnx_sql.execute(query)
    return cnx_sql.fetchone()