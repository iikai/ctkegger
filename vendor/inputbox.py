# by Timothy Downs, inputbox written for my map editor

# This program needs a little cleaning up
# It ignores the shift key
# And, for reasons of my own, this program converts "-" to "_"

# A program to get user input, allowing backspace etc
# shown in a box in the middle of the screen
# Called by:
# import inputbox
# answer = inputbox.ask(screen, "Your name")
#
# Only near the center of the screen is blitted to

import pygame, pygame.font, pygame.event, pygame.draw, string
from pygame.locals import *

FONTNAME = './assets/fonts/prstartk.ttf'

def get_key():
  while 1:
    event = pygame.event.poll()
    if event.type == KEYDOWN:
      return (event.key, event.unicode)
    else:
      pass

def display_question(screen, message):
  "Print a message in a box in the middle of the screen"
  fontobject = pygame.font.Font(FONTNAME,24)
  pygame.draw.rect(screen, (0,0,0),
                   ((screen.get_width() / 2) - 400,
                    (screen.get_height() / 2) - 10,
                    800,70), 0)
  pygame.draw.rect(screen, (255,255,255),
                   ((screen.get_width() / 2) - 402,
                    (screen.get_height() / 2) - 12,
                    804,72), 2)
  if len(message) != 0:
    screen.blit(fontobject.render(message, 1, (255,255,255)),
                ((screen.get_width() / 2) - 390, (screen.get_height() / 2) - 5))
  pygame.display.flip()
def display_input(screen, message):
   "Print a message in a box in the middle of the screen"
   fontobject = pygame.font.Font(FONTNAME,24)

   if len(message) != 0:
     screen.blit(fontobject.render(message, 1, (255,255,255)),
                 ((screen.get_width() / 2) - 390, (screen.get_height() / 2) + 28))
   pygame.display.flip()


def ask(screen, question):
    # "ask(screen, question) -> answer"
    pygame.font.init()
    current_string = []
    new = "".join(current_string)
    display_question(screen, question + " " + new.join(""))
    display_input(screen, ">>" + new.join(""))
    while True:
        (inkey, unichar) = get_key()
        if inkey == K_BACKSPACE:
            current_string = current_string[0:-1]
        elif inkey == K_RETURN:
            output = new.join(current_string)
            break
        elif inkey == K_ESCAPE:
            output = None
            break
        else:
            current_string.append(unichar)
        display_question(screen, question + " " + new.join(""))
        display_input(screen, ">>" + new.join(current_string))
    return output


def askNumber(screen, question):
    # "ask(screen, question) -> answer"
    pygame.font.init()
    current_string = []
    new = "".join(current_string)
    display_question(screen, question + " " + new.join(""))
    display_input(screen, ">>" + new.join(""))
    while True:
        (inkey, unichar) = get_key()
        if inkey == K_BACKSPACE:
            current_string = current_string[0:-1]
        elif inkey == K_RETURN:
            output = new.join(current_string)
            break
        elif inkey == K_ESCAPE:
            output = None
            break
        elif str(unichar).isnumeric() or str(unichar) == '.':
            current_string.append(unichar)
        else:
            pass
        display_question(screen, question + " " + new.join(""))
        display_input(screen, ">>" + new.join(current_string))
    return output


def main():
    screen = pygame.display.set_mode((640,480))
    print(ask(screen, "Name") + " was entered")

if __name__ == '__main__': main()
