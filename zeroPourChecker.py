# -*- coding: iso-8859-1 -*-
import sys
import schedule
import time
import datetime
import json
import redis
import utils.apiCalls as ac
from utils.dbConnection import Database
# set up database
countCnx = Database('ctkegger')
# set up REDIS
redisServer = redis.Redis(host='localhost', port=6379, db=0)
print('started zeroPourChecker.py.')
sys.stdout.flush()

def createZeroRecord(time, lineType):
    zeroRecord = {
        'drinkerName':  'ZERO',
        'consumedQty':  '0',
        'date':  time.strftime('%Y-%m-%dT00:00:00Z'),
        'lineType':  lineType,
        'dateTime':  time.strftime('%Y-%m-%dT%TZ')
    }
    if lineType == 'Pour':
        zeroRecord['beerDescription'] = 'NO BEER'
    return [zeroRecord]

def checkPourCount():
    print('starting to run checkPourCount()')
    sys.stdout.flush()
    timeNow = datetime.datetime.now()
    timeForZeroRecord = timeNow - datetime.timedelta(minutes=30)
    currentTime = timeNow.strftime('%Y-%m-%d %H:%M:00')
    args = [currentTime, ]
    query = 'SELECT COUNT(id) from Pour where poured_at > %s - INTERVAL 1 HOUR'
    countCnx.execute(query, args)
    counts = countCnx.fetchone()
    if counts['COUNT(id)'] == 0:
        print(f'No pours during last hour: {currentTime}')
        sys.stdout.flush()
        jsonData = createZeroRecord(timeForZeroRecord, 'Pour')
        if redisServer.llen('pours') == 0:  # There is no queue in REDIS, all good
            ac.postJsonToAPI(jsonData, True, 'pours')
        else:  # There is a queue in REDIS -> Get at the back of the queue
            print(f"Pushing data to REDIS due to a queue in REDIS (length: {redisServer.llen('pours')}): {jsonData}")
            sys.stdout.flush()
            redisServer.rpush('pours', json.dumps(jsonData))

        jsonData = createZeroRecord(timeForZeroRecord, 'Serving')
        if redisServer.llen('servings') == 0:  # There is no queue in REDIS, all good
            ac.postJsonToAPI(jsonData, True, 'servings')
        else:  # There is a queue in REDIS -> Get at the back of the queue
            print(f"Pushing data to REDIS due to a queue in REDIS (length: {redisServer.llen('servings')}): {jsonData}")
            sys.stdout.flush()
            redisServer.rpush('servings', json.dumps(jsonData))
    else:
        print(f"{counts['COUNT(id)']} pours during last hour: {currentTime}")
        sys.stdout.flush()
    sys.stdout.flush()

# schedule.every(5).minutes.do(checkPourCount)
# schedule.every().minute.do(checkPourCount)
schedule.every().hour.at(':00').do(checkPourCount)

while True:
    schedule.run_pending()
    time.sleep(1)
sys.exit
